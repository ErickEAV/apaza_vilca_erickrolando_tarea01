#include "chess.h"
#include "figures.h"

void display(){
  char** blackSquare = reverse(whiteSquare);
  
  char** tokens = join(rook,join(knight,join(bishop,join(queen,join(king,join(bishop,join(knight,rook)))))));
  char** rawPawn = repeatH(pawn, 8);
  
  char** raw = repeatH(join(blackSquare,whiteSquare),4);
  char** dualraw = up(raw,reverse(raw));

  char** uptokensTable = superImpose(up(tokens, rawPawn),dualraw);
  char** downtokensTable = reverse(superImpose(up(rawPawn,tokens), reverse(dualraw)));
  
  char** table = up(uptokensTable, up(repeatV(dualraw,2), downtokensTable));
 
  interpreter(table);

}
