#include "chess.h"
#include "figures.h"

void display(){
  char** blackKnight = superImpose(reverse(knight), whiteSquare);
  char** downPawn = flipH(superImpose(reverse(pawn), whiteSquare));
  char** leftPawn = rotateL(superImpose(reverse(pawn), whiteSquare));
  char** rightPawn = rotateR(superImpose(reverse(pawn), whiteSquare));
  char** overPawn = superImpose(reverse(pawn), whiteSquare);
  char** raw1 = join(whiteSquare, join(blackKnight, join(whiteSquare, whiteSquare)));
  char** raw2 = join(rotateR(flipH(blackKnight)), join(downPawn, join(leftPawn, whiteSquare)));
  char** raw3 = join(whiteSquare, join(rightPawn, join(overPawn, rightPawn)));
  char** raw4 = join(whiteSquare, join(whiteSquare, join(downPawn, whiteSquare)));

  char** quartern = up(raw1, up(raw2, up(raw3, raw4)));
  char** half = join(quartern, flipV(quartern));
  char** table = up(half, flipH(half));
 
  interpreter(table);
}
