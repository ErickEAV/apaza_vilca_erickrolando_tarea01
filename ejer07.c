#include "chess.h"
#include "figures.h"

void display(){
  char** blackSquare = reverse(whiteSquare);																					
  char** raw1 = join(whiteSquare, join(blackSquare, join(whiteSquare, join(superImpose(queen, blackSquare), whiteSquare))));//quuen-4,1
  char** raw2 = join(superImpose(queen, blackSquare), join(whiteSquare, join(blackSquare, join(whiteSquare, blackSquare))));//queen-1,2
  char** raw3 = join(whiteSquare, join(blackSquare, join(superImpose(queen, whiteSquare), join(blackSquare, whiteSquare))));//queen-3,3
  char** raw4 = join(blackSquare, join(whiteSquare, join(blackSquare, join(whiteSquare, superImpose(queen, blackSquare)))));//queen-4,5
  char** raw5 = join(whiteSquare, join(superImpose(queen, blackSquare), join(whiteSquare, join(blackSquare, whiteSquare))));//queen-5,2

  char** table = up(raw1, up(raw2, up(raw3, up(raw4, raw5))));
 
  interpreter(table);
}