#include "chess.h"
#include "figures.h"

void display(){
  char** blackSquare = reverse(whiteSquare);
  char** raw = repeatH(join(blackSquare,whiteSquare),4);
  char** tokens = join(rook,join(knight,join(bishop,join(queen,join(king,join(bishop,join(knight,rook)))))));
  char** overhead = superImpose(tokens,raw);  
  interpreter(overhead);
}
