#include "chess.h"
#include "figures.h"

void display(){
  char** blackSquare = reverse(whiteSquare);
  char** raw1 = repeatH(join(blackSquare,whiteSquare),4);
  char** raw2 = repeatH(join(whiteSquare,blackSquare),4);
  char** columns = repeatV(up(raw1,raw2),2);
  interpreter(columns);
}
